OPTIONS = -O2 -fopenmp
EXEC = Astro_Nbody
OBJS = main.o
INCL = vmath.h Makefile
CPPFLAGS = $(OPTIONS)
CC = g++

.cpp.o:
	$(CC) -c $(CPPFLAGS) -o "$@" "$<"

$(EXEC): $(OBJS)
	$(CC) $(OPTIONS) $(OBJS) -o $(EXEC)

$(OBJS): $(INCL)

clean:
	rm -f $(OBJS) $(EXEC)
