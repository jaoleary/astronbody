// A set of functions for defining and operating on vectors

class vec{
	public:
	double comp[3];
	vec(){comp[0]=comp[1]=comp[2]=0;}
	vec(double _x, double _y, double _z) {
		comp[0] = _x;
		comp[1] = _y;
		comp[2] = _z;
	}

	double dot(vec _x) {
		return (comp[0]*_x.comp[0] + comp[1]*_x.comp[1] + comp[2]*_x.comp[2]);
	}

	double abs() {
		return (sqrt(comp[0]*comp[0] + comp[1]*comp[1] + comp[2]*comp[2]));
	}

	vec cross(vec _x) {
		return(vec(
			comp[1]*_x.comp[2] - _x.comp[1]*comp[2],
			_x.comp[0]*comp[2] - comp[0]*_x.comp[2],
			comp[0]*_x.comp[1] - _x.comp[0]*comp[1]));
	}

	vec operator*(double _x) {
		return(vec(
			comp[0]*_x,
			comp[1]*_x,
			comp[2]*_x));
	}

	vec operator/(double _x) {
		return(vec(
			comp[0]/_x,
			comp[1]/_x,
			comp[2]/_x));
	}

	vec operator+(vec _x) {
		return(vec(
			comp[0] + _x.comp[0],
			comp[1] + _x.comp[1],
			comp[2] + _x.comp[2]));
	}

	vec operator-(vec _x) {
		return(vec(
			comp[0] - _x.comp[0],
			comp[1] - _x.comp[1],
			comp[2] - _x.comp[2]));
	}
};


std::ostream &operator << (ostream &os, const vec& _x) { os 	<< ' ' << _x.comp[0]
								<< ' ' << _x.comp[1]
								<< ' ' << _x.comp[2]
								<< ' ';
}

vec operator * (double _a, const vec& _x) {
	return(vec(
		_a*_x.comp[0],
		_a*_x.comp[1],
		_a*_x.comp[2]));
}

struct state{
	/*
	a structure containg the state of all particles in the system
	*/
	vec pos, vel, acc;
	double t, m;
	state() { pos=vel=acc=vec(0,0,0); m=t=0; }
	state(double _x,double _v,double _m) { pos=vel=acc=vec(0,0,0); t=0; pos.comp[0]=_x; vel.comp[1]=_v; m=_m;}
};
