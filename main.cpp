
#include<iostream>
#include<iomanip>
#include<fstream>
#include<sstream>
#include<cmath>
#include<cstdlib>
#include<vector>
#include<string>
#include<ctime>
#include<omp.h>

using namespace std;

#include"vmath.h"

/*User inputs*/
double const G = 1;
const double dt0 = 0.00001 ;
const double STEPS = 100000 ;
const double TMAX = STEPS*dt0 ;
const int NUMPARTS = 800;

const string BASENAME = "snapshot";
const string OUTDIR = "./output/";
const string INDIR = "./ICs/";

const double SOFTENING  = 0.0;

void IC_fromfile_xvm(vector<state> &start, string fname) {
	/*
	Import ICs from file.
	*/
	int line_num = 0;
	string line;
	ifstream infile;

	infile.open(fname.c_str());

	if (infile.is_open()) {
		while (getline(infile,line) && line_num < NUMPARTS) {
			stringstream ss(line);
			double x,y,z,vx,vy,vz,m;
			ss >> x >> y >> z >> vx >> vy >> vz >> m;
			state temp;
			temp.m = m;
			temp.t = 0;
			temp.pos = vec(x,y,z);
			temp.vel = vec(vx,vy,vz);
			temp.acc = vec(0,0,0);
			start.push_back(temp);
			line_num += 1;
		}
		infile.close();
	}
	else cout << "Unable to open file: " << fname << endl;
	cout << "# Read " << start.size() << " points!" << endl;
}


void accel(vector<state> &here) {
/*
Compute acceleration of a system of particles under mutual gravitational attraction.
*/
#pragma omp parallel for
	for (int i = 0; i < here.size(); i++) {
		here[i].acc = vec(); // initialize with zeros
		for (int j = 0; j < here.size(); j++) {
			if (i != j) {
				vec dx = here[i].pos - here[j].pos;
				double r  = sqrt(dx.abs()*dx.abs() + SOFTENING*SOFTENING);
				here[i].acc = here[i].acc + dx * (-G*here[j].m/(pow(r,3)));
			}
		}
	}
}

void kick(vector<state> &here, double dt) {
/*
Update velocity of a system of particles using their current acceleration
*/
#pragma omp parallel for
	for (int i = 0; i < here.size(); i++) {
		here[i].vel = here[i].vel + here[i].acc * dt;
	}
}

void drift(vector<state> &here, double dt) {
/*
Update position for a system of particles using their current velocity
*/
#pragma omp parallel for
	for (int i = 0; i < here.size(); i++) {
		here[i].pos = here[i].pos + here[i].vel*dt;
	}
}

int main(int argc, const char **argv) {
	srand (time(NULL));
	vector<state> system;
	double print_time = 0.0;
	double dt = 0;
	int outnum = 0;
	int outputFreq = 10000;
	int timeStepCounter = 0;

	ofstream outfile;
#pragma omp parallel
{
#pragma omp master
{
	cout << "Using " << omp_get_num_threads() << " OpenMP threads..." << endl;
	}
}
	IC_fromfile_xvm(system,INDIR+"system.txt");

	accel(system);
	double current_time = system[0].t;

	while (system[0].t < TMAX) {

/*
//set for even time steps according to the particle requiring smallest timestep
		double a_max = 0;
		for (int n = 0; n < system.size(); n++) {
			if (system[n].acc.abs() > a_max) {
				a_max = system[n].acc.abs();
			}
		}

		dt = dt0 / a_max;
*/
		dt = dt0;

		if(timeStepCounter%outputFreq == 0) {
			//string name = OUTDIR + BASENAME + "_" + (string)outnum;
			stringstream ss;
			ss << OUTDIR << BASENAME << "_" << setw(4) << setfill('0') << outnum;
			string fname = ss.str();
			cout << "Writing t = " << current_time << endl;

			outfile.open(fname.c_str());
			outfile << "# ID, X, Y, Z, V_x, V_y, V_z, mass" << endl;

			for (int n = 0; n < system.size(); n++){
				outfile  << n << ' '
					<< system[n].pos
					<< system[n].vel
					<< system[n].m
					<< endl;
			}
			outfile.close();
			outnum += 1;
		}

		/*move the particle with KDK*/
		kick(system,0.5*dt);
		drift(system,dt);
		accel(system);
		kick(system,0.5*dt);

		/*update particle time*/
		/* not needed for even time steps
#pragma omp parallel for
		for (int n = 0; n < system.size(); n++){
			system[n].t = system[n].t + dt;
		}
		*/
    system[0].t = system[0].t + dt;
		/*update simulation timestep*/
		current_time = system[0].t;
		timeStepCounter += 1;

	}

	return(0);
}
