simple parallel N-body code for the 2018 Summer School in Astroinformatics at Penn State. Code originally written as part of a course taught by Andreas Burkert at LMU/USM Munich.

This code utilizes the GNU GENERAL PUBLIC LICENSE Version 3.

Primary author: Joseph O'Leary (joleary@usm.lmu.de) 
